#!/bin/bash

if [[ -z "$1" ]]; then
	echo "Usage: $0 domain.local"
	echo "  - generate self signed certificate for nginx."
	echo "  - .local part is required"
	exit 1
fi

DOMAIN=$1

echo "Generating certificates for $DOMAIN"

openssl req -x509 -out $DOMAIN.crt -keyout $DOMAIN.key \
	-newkey rsa:2048 -nodes -sha256 -subj '/CN=localhost' \
	-extensions EXT \
	-config <( printf "[dn]\nCN=localhost\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:$DOMAIN\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")
