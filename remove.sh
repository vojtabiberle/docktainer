#!/usr/bin/env bash

GREEN=$(echo -en '\033[00;32m')
YELLOW=$(echo -en '\033[00;33m')
RESTORE=$(echo -en '\033[0m')

test -f .env && source .env

docker-compose rm -f 1> /dev/null