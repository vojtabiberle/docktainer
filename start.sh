#!/usr/bin/env bash

test -f .env && source .env

docker-compose up -d --remove-orphans 1> /dev/null