# Docktainer

## Portainer local web development environment

This repository contains scripts for easy use [Portainer](https://www.portainer.io/) and [docker-hostmanager](https://github.com/vojtabiberle/docker-hostmanager) together.

You can easily run multiple local dockerized web servers and multiple databases with nice Portainer UI on top of it.

## Usage

1. clone this repository
   
   ```git clone https://gitlab.com/vojtabiberle/docktainer```

2. stop all currently running webservers and databases
3. modify `docker-compose.yml` files in your projects
4. run `./start.sh` script

## FAQ

### How I can start everything?

```bash
./start.sh
```

### How I stop everything?

```bash
./stop.sh
```

### How I remove everything?

```bash
./remove.sh
```

### Do I need start everything after every reboot of computer?

No. Docker do everything for you.

### Where I can find portainer UI?

Just take look at [http://portainer.docker](http://portainer.docker)

### What I can do, when my colleagues don't want use Docktainer?

You can copy project `docker-compose.yml` do your own `docker-compose.local.yml` and set in your `.env` variable `COMPOSE_FILE`.

When you want override:

```env
COMPOSE_FILE=docker-compose.yml:docker-compose.local.yml
```

When you want use only your file:

```env
COMPOSE_FILE=docker-compose.local.yml
```

And then add `docker-compose.local.yml` to `.gitignore` or just commit it to repo. Maybe one colleague give it try ;-)

## Licence

All my sources ale under MIT licence.

Copyright (c) 2019 Vojtěch Biberle